/*
** EPITECH PROJECT, 2017
** infin_add.c
** File description:
** 
*/

#include <unistd.h>
#include <stdlib.h>
#include "my_putchar.c"
#include "my_strlen.c"

int *infin_add(char *str)
{
	int i;
	int j;
	int k;
	char *res;

	j = 0;
	k = 0;
	res = malloc(sizeof(*res) * my_strlen(str + 1));
	while(str[i] = '0')
		i = i + 1;
	while(str[i] != '\0') {
		if (str[i] < 48 || str[i] > 57)
			i = i + 1;
		else {
			j = j + 1;
			i = i + 1;
			k = 1;
		}
	}
	if (k == 0)
		res[0] = '0';
	return(res);
}
// place la fonction du teams ici stp :

int my_putstr_inf(char *str)
{
	int i;
	i = 0;
	
	while(str[i] != '\0') {
		my_putchar(str[i]);
		i = i + 1;
		}

	my_putchar('\n');
	return(0);
}

int main_infin_add(char *argv1, char *argv2)
{
	int j;
	int k;

	j = my_strlen(argv1);
	k = my_strlen(argv2);
	return(my_putstr_inf(infin_add(argv1, argv2, j, k)));
}

int my_strcmp(char const *s1, char const *s2)
{
	int i;

	i = 0;
	while(s1[i] == s2[i])
		i = i + 1;
	return(s1[i] - s2[i]);
}

int len(char *nb1, char *nb2)
{
	int a;
	int b;

	a = my_strlen(nb1);
	b = my_strlen(nb2);
	if(a > b)
		return(a);
	else
		return(b);
}

int main(char argc, char **argv)
{
	int j;
	int k;

	if (argc != 3)
		return(write(2, &"Usage; ./infin_add number1 number2\n", 36));
	else
		main_infin_add(argv[1], argv[2], j, k);
	my_putchar('\n');
	return(0);
}
