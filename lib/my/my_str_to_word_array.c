/*
** EPITECH PROJECT, 2017
** task04
** File description:
** my_str_to_word_array
*/

#include <stdlib.h>
#include <stdio.h>

char **my_str_to_word_array(char const *str)
{
	char **tab;
	int i = 0;
	int n = 0;
	int b = 0;
	int c = 0;
	int a = 0;

	while (str[i] != '\0') {
		if (str[i] == ' ')
			n = n + 1;
		i = i + 1;
	}
	i = 0;
	tab = malloc(sizeof(char *) * n);
	while (str[i] != '\0') {
		if (c == 0) {
			tab[b] = malloc(sizeof(char) * 100);
			a = 0;
		}
		tab[b][c] = str[i];
		c = c + 1;
		if (str[i] == ' ') {
			tab[b][c] = '\0';
			b = b + 1;
			c = 0;
		}
		i = i + 1;
	}
	return tab;

}
