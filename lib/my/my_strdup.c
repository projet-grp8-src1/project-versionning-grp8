/*
** EPITECH PROJECT, 2017
** my_strdup.c
** File description:
** 
*/
#include<stdlib.h>

char *my_strdup(char const *src)
{
	int i = 0;
	int e = my_strlen(src);
	char *str;
	
       	str = malloc(sizeof(char) * (e+1));
	while(src[i] != '\0') {
		str[i] = src[i];
		i = i + 1;
	}
	str[i]='\0';
	return(str);
}
