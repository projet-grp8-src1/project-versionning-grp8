char *my_strncpy(char *dest, char const *src, int n)
{
	int i = 0;

	if(n >= 1) {
		while(src[i] != 0) {
			dest[i] = src[i];
			i = i + 1;
			--n;
		}

		return(dest);
	}else
	{
		while(src[i] != 0) {
			dest[i] = src[i];
			i = i + 1;
			--n;
		}
	}
	dest[i] = '\0';
	return(dest);
}
