void my_swap(int *a, int *b)
{
        int swap = *a;

        a = *a;
        *a = *b;
        *b = swap;
}
