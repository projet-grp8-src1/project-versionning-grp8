int my_strlen(char const *str)
{
        int n;

        n = 0;
        while(str[n] != '\0') {
                n = n + 1;
        }

        return(n);
}
